<?php

namespace App\Http\Controllers;

use App\Models\Pids;
use Illuminate\Http\Request;
use Inertia\Inertia;

class PidsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pids = Pids::all();
        return Inertia::render('Home', compact('pids'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $pids = Pids::all();
        return Inertia::render('InputData', compact('pids'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'datetime' => 'required',
            'activity_description' => 'required',
            'note' => 'required',
        ]);
        $created = Pids::create($validated);
        if($created){
            
            return redirect()->back()->with('message', 'PID added successfully');
        }

        return redirect()->back()->with('message', 'Failed to add PID');
    }

    /**
     * Display the specified resource.
     */
    public function show(Pids $pids)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Pids $pids)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Pids $pids)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id, Pids $pids)
    {
        $deleted = $pids->find($id)->delete();

        if($deleted){
            return redirect()->back()->with('message', 'PID deleted successfully');
        }

        return redirect()->back()->with('message', 'Failed to delete PID');
    }
}
