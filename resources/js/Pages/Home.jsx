import Each from "@/Components/Each";
import Clock from "@/Components/Partials/Clock";
import MainComponent from "@/Layouts/MainComponent";
import { pairData } from "@/Utils/Model/InputModel";
import { Button } from "antd";
import React from "react";
import { FiEdit } from "react-icons/fi";
import TNU from '@/Assets/TNU.png';

function Home({ pids }) {
    console.log({ pids });
    return (
        <MainComponent>
            <div className=" w-full justify-end">
                <div className="fixed top-0 left-20 z-50">
                    <img
                        src={TNU}
                        className="w-36 h-3w-36 rounded-full"
                    />
                </div>
                <div className="fixed top-0 right-10 z-50">
                    <Clock pids={pids} />
                </div>

                <div className="max-h-[80vh] overflow-y-auto flex justify-center  p-5 mt-28">
                    <table className="overflow-auto border-collapse w-full drop-shadow-xl shadow-md">
                        <thead className="sticky -top-5">
                            <tr>
                                <Each
                                    of={pairData({})}
                                    render={(data) => {
                                        return (
                                            <th className="p-3 font-bold uppercase bg-gray-200 text-gray-600 border border-gray-300 hidden lg:table-cell">
                                                {data.text}
                                            </th>
                                        );
                                    }}
                                />
                            </tr>
                        </thead>
                        <tbody>
                            <Each
                                of={pids.sort((a, b) => {
                                    let timeA = a.datetime
                                        .split(":")
                                        .map(Number);
                                    let timeB = b.datetime
                                        .split(":")
                                        .map(Number);

                                    if (timeA[0] === timeB[0]) {
                                        return timeA[1] - timeB[1];
                                    } else {
                                        return timeA[0] - timeB[0];
                                    }
                                })}
                                render={(data) => {
                                    return (
                                        <tr
                                        className="bg-white odd:bg-slate-50"
                                        //  className="bg-white lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                                         >
                                            <Each
                                                of={pairData({ val: data })}
                                                render={(fn) => {
                                                    console.log({ fn });
                                                    return (
                                                        <td
                                                            className={`${fn.addtionalClass} py-0   text-gray-800 text-center border border-b block lg:table-cell relative lg:static`}
                                                        >
                                                            {/* <span className="lg:hidden py-0 absolute top-0 left-0 bg-blue-200 px-2 text-xs font-bold uppercase">
                                                                {fn.text}
                                                            </span> */}
                                                            <>{fn.value}</>
                                                        </td>
                                                    );
                                                }}
                                            />
                                        </tr>
                                    );
                                }}
                            />
                        </tbody>
                    </table>
                </div>
            </div>
        </MainComponent>
    );
}

export default Home;
