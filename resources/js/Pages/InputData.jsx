import Each from "@/Components/Each";
import Input from "@/Components/Form/Input";
import { Toast } from "@/Components/Swal/Swal";
import MainComponent from "@/Layouts/MainComponent";
import { pairData } from "@/Utils/Model/InputModel";
import { router } from "@inertiajs/react";
import { Button, Empty, FloatButton, TimePicker } from "antd";
import dayjs from "dayjs";
import React, { useEffect, useState } from "react";
import { FiDelete, FiEdit, FiPlus } from "react-icons/fi";
import { RiDeleteBin2Line, RiDeleteBinLine } from "react-icons/ri";
import TextSpeech from "@/Components/Partials/TextSpeech";

function InputData({ pids, flash }) {
    console.log({ pids });

    useEffect(() => {
        flash.message && Toast({ title: flash.message, icon: "success" });
    }, [flash]);

    const time = new Date();
    
    const hours = time.getHours()
    const minutes = time.getMinutes()
    const initPyload = {
        datetime: `${hours}:${minutes}`,
        activity_description: "",
        note: "",
        get hours() {
            return this.datetime.split(":")[0];
        },
        get minutes() {
            return this.datetime.split(":")[1];
        },
    };
    const [payload, setPayload] = useState(initPyload);
    console.log({ payload });
    const handleSubmit = (e) => {
        e.preventDefault();
        router.post("/inputdata", payload);
        setPayload(initPyload);
    };

    const handleDelete = (id) => {
        router.delete("/inputdata/" + id);
    };

    const handleChange = (e) => {
        setPayload((prev) => {
            return { ...prev,
                 [e.target.id]: e.target.value 
                };
        });
    };
    return (
        <MainComponent>
            
            {/* <TextSpeech
                text={"Input Data Test 1234567890"}
            /> */}
            <form onSubmit={handleSubmit} className="my-5 lg:px-36 md:gap-5 flex sticky top-0">
                <div className="flex justify-between form-input py-1 rounded w-1/2">
                    <Input
                    showSearch
                    inputType="selectAntd"
                    id={"hours"}
                    onChange={val => 
                        setPayload((prev) => {
                            return { ...prev,
                                 hours: val,
                                 get datetime() {
                                     return `${this.hours ?? 0}:${this.minutes ?? '00'}`
                                 }
                                };
                        })}
                    value={payload?.hours}
                    options={Array.from({length: 24}, (_, i) => i.toString().padStart(2, '0'))?.map((data) => ({
                        value: data,
                        label: data,
                      }))}
                    addtionalClass="h-full text-xs  border-none w-full" 
                    />
                    <Input
                    inputType="selectAntd"
                    id={"minutes"}
                    onChange={val => 
                        setPayload((prev) => {
                            return { ...prev,
                                 minutes: val,
                                 get datetime() {
                                     return `${this.hours ?? 0}:${this.minutes ?? '00'}`
                                 }
                                };
                        })}
                    value={payload?.minutes}
                    options={Array.from({length: 60}, (_, i) => i.toString().padStart(2, '0'))?.map((data) => ({
                        value: data,
                        label: data,
                      }))}
                    addtionalClass="h-full text-xs  border-none w-full" 
                    />

                </div>
                <Input
                    placeholder={"Nama Kegiatan"}
                    addtionalClass="shadow-inner rounded"
                    id={"activity_description"}
                    value={payload?.activity_description}
                    onChange={handleChange}
                />
                <Input
                    placeholder="Keterangan"
                    addtionalClass="shadow-inner rounded"
                    id={"note"}
                    value={payload?.note}
                    onChange={handleChange}
                />
                <Button htmlType="submit" className="border-none self-center">
                    <FiPlus className="rounded-xl" />
                </Button>
            </form>
            <table className="border-collapse w-full drop-shadow-xl max-h-[50vh]  shadow-md">
                <thead>
                    <tr>
                        <Each
                            of={pairData({})}
                            render={(data) => {
                                return (
                                    <th className="p-3 font-bold uppercase bg-gray-200 text-gray-600 border border-gray-300 hidden lg:table-cell">
                                        {data.text}
                                    </th>
                                );
                            }}
                        />

                        <th className="p-3 font-bold uppercase bg-gray-200 text-gray-600 border border-gray-300 hidden lg:table-cell">
                            Actions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <Each
                        of={pids.sort((a, b) => {
                            let timeA = a.datetime.split(':').map(Number);
                            let timeB = b.datetime.split(':').map(Number);
                            
                            if (timeA[0] === timeB[0]) {
                                return timeA[1] - timeB[1];
                            } else {
                                return timeA[0] - timeB[0];
                            }
                        }).reverse()
                        }
                        render={(data) => {
                            return (
                                <tr className="bg-white lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0">
                                    <Each
                                        of={pairData({ val: data })}
                                        render={(fn) => {
                                            return (
                                                <td className="w-full py-0 lg:w-auto p-3 text-gray-800 text-center border border-b block lg:table-cell relative lg:static">
                                                    <span className="lg:hidden py-0 absolute top-0 left-0 bg-blue-200 px-2 text-xs font-bold uppercase">
                                                        {fn.text}
                                                    </span>
                                                    <>{fn.value}</>
                                                </td>
                                            );
                                        }}
                                    />

                                    <td className="w-full flex justify-center gap-1">
                                        {/* <Button className="text-blue-400 hover:text-blue-600 underline">
                                            <FiEdit />
                                        </Button> */}
                                        <Button
                                            onClick={() => handleDelete(data.id)}
                                            icon={<RiDeleteBin2Line />}
                                            className="text-blue-400 hover:text-blue-600 underline "
                                        >
                                            {" "}
                                        </Button>
                                    </td>
                                </tr>
                            );
                        }}
                    />
                </tbody>
            </table>


          {!pids.length  &&  <Empty className="mt-10"/>}
        </MainComponent>
    );
}

export default InputData;
