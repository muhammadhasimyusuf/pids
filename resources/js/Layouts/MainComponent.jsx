import Each from '@/Components/Each'
import { menu } from '@/Utils/helper'
import { Link } from '@inertiajs/react'
import React from 'react'
import bg from '@/Assets/bg.jpg';

function MainComponent({
    children
}) {
  const path = window.location.pathname;
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const changeable = urlParams.get('changeable');
  return (
<div className="h-screen w-full bg-white relative flex overflow-hidden">


{changeable && <aside className="h-full w-16 flex flex-col space-y-10 items-center justify-center relative bg-gray-800 text-white">
    <Each
      of={menu}
      render={
        (item, index) => {
          return <Link
          href={!changeable ? item.link : `${item.link}?changeable=true`}
          className={`h-10 w-10 flex items-center justify-center rounded-lg cursor-pointer hover:text-gray-800 hover:bg-white hover:shadow-inner  hover:duration-300 hover:ease-linear focus:bg-white ${path === item.link ? 'bg-gray-500 shadow-inner' : ''}`}>
          <item.icon className='text-xl font-bold'/>
         </Link>
        }}
    />
  </aside>
}
  
 
  <div className="w-full h-full flex flex-col justify-between bg-black">
    {/* <header className="h-16 w-full flex items-center relative justify-end px-5 space-x-10 bg-gray-800">
      <div className="flex flex-shrink-0 items-center space-x-4 text-white">
        <div className="flex flex-col items-end ">
     
          <div className="text-md font-medium ">Unknow Unknow</div>
   
        </div>
        
        <div className="h-10 w-10 rounded-full cursor-pointer bg-gray-200 border-2 border-blue-400"></div>
      </div>
    </header> */}

    <main
    // style={{ 
    //   backgroundImage : `url(${bg})`,
    //   backgroundSize : 'cover',
    //   backgroundRepeat : 'no-repeat'
    //  }}
    className="max-w-full h-full flex relative overflow-y-hidden bg-gray-100">
    <div
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundImage: `url(${bg})`,
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            filter: 'blur(8px)',
            // zIndex: -1,
          }}
        >
          
        </div>
      <div className="max-h-screen overflow-y-auto w-full m-4  gap-4 ">
        {children}
        {/* <div className="w-96 h-60 rounded-lg flex-shrink-0 flex-grow bg-gray-400"></div>
        <div className="w-96 h-60 rounded-lg flex-shrink-0 flex-grow bg-gray-400"></div>
        <div className="w-96 h-60 rounded-lg flex-shrink-0 flex-grow bg-gray-400"></div>
        <div className="w-96 h-60 rounded-lg flex-shrink-0 flex-grow bg-gray-400"></div>
        <div className="w-96 h-60 rounded-lg flex-shrink-0 flex-grow bg-gray-400"></div>
        <div className="w-96 h-60 rounded-lg flex-shrink-0 flex-grow bg-gray-400"></div>
        <div className="w-96 h-60 rounded-lg flex-shrink-0 flex-grow bg-gray-400"></div>
        <div className="w-96 h-60 rounded-lg flex-shrink-0 flex-grow bg-gray-400"></div>
        <div className="w-96 h-60 rounded-lg flex-shrink-0 flex-grow bg-gray-400"></div> */}
      </div>
    </main>
  </div>

</div>
  )
}

export default MainComponent