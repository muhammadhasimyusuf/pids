import React, { useState, useEffect, useRef } from 'react';
import { showAlert } from '../Swal/Swal';
import { isFiveMinutesBefore, timeCheck } from './Func';
import TextToSpeech from './TextSpeech';

const Clock = ({pids}) => {
  const [time, setTime] = useState(new Date());
  const [prevTime, setPrevTime] = useState({ hours: '', minutes: '', seconds: '' });
  const [isAlertShown, setIsAlertShown] = useState(false);
  const [isAlert10Shown, setIsAlert10Shown] = useState(false);
  const [text, setText] = useState('');
  const playRef = useRef(null)
  const playSpeech = data => {
    console.log('speeeeech')
    console.log({data})
    data.click()
  }

  useEffect(() => {
    const intervalId = setInterval(() => {
      setTime(new Date());
    }, 1000);

    return () => clearInterval(intervalId);
  }, []);
console.log({isAlertShown})
  useEffect(() => {
    setPrevTime({
      hours: time.getHours(),
      minutes: time.getMinutes(),
      seconds: time.getSeconds(),
    });


    !isAlert10Shown && pids.forEach(fn => {
      if(timeCheck(fn.datetime, time, 10, setIsAlert10Shown)) {
        console.log("harusnya hanya 1")
        setIsAlert10Shown(true)
        setText(`10 Menit Sebelum Kegiatan ${fn.activity_description}`)
        playSpeech(playRef.current)
        showAlert({
          title : `10 Menit Sebelum Kegiatan ${fn.activity_description}`,
          icon : 'warning',
        }).then(() => {
          playSpeech(playRef.current)
          setTimeout(() => {
            setIsAlert10Shown(false)
          }, 1000 * 59);
        })
      }
    })
    !isAlertShown && pids.forEach(fn => {
      if(timeCheck(fn.datetime, time, 0, setIsAlertShown)) {
        console.log("harusnya hanya 1")
        setIsAlertShown(true)
        setText(`Waktunya Kegiatan ${fn.activity_description}`)
        playSpeech(playRef.current)
        showAlert({
          title : `Waktunya Kegiatan ${fn.activity_description}`,
          icon : 'warning',
        }).then(() => {
          playSpeech(playRef.current)
          setTimeout(() => {
            setIsAlertShown(false)
          }, 1000 * 59);
        })
      }
    })
  }, [time]);

  const formatTime = (date) => {
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');
    const seconds = String(date.getSeconds()).padStart(2, '0');
    return { hours, minutes, seconds };
  };

  const formatDate = (date) => {
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    return date.toLocaleDateString('en-US', options);
  };

  const { hours, minutes, seconds } = formatTime(time);
  const date = formatDate(time);

  const getClassName = (unit, prevUnit) => {
    return unit !== prevUnit ? 'inline-block transform-gpu animate-flip' : 'inline-block';
  };

  return (
    <div className="flex items-center justify-end ">
      <div hidden> <TextToSpeech text={text} playRef={playRef}/> </div>
      <div className=" p-8 rounded-lg shadow-lg text-center text-white mb-4">
        <div className="text-3xl hover:text-5xl font-bold transition duration-200 ease-in-out">
          <span className={getClassName(hours, prevTime.hours)}>{hours}</span>:
          <span className={getClassName(minutes, prevTime.minutes)}>{minutes}</span>:
          <span className={getClassName(seconds, prevTime.seconds)}>{seconds}</span>
        </div>
      <div className="text-white">{date}</div>
      </div>
    </div>
  );
};

export default Clock;
