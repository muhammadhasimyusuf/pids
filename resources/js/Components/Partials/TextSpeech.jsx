import React, { useState, useEffect } from "react";

const getVoices = async () => {
  const synth = window.speechSynthesis;
  const voices = await synth.getVoices();
  return voices;
};

const TextToSpeech = ({ text, playRef }) => {
  const synth = window.speechSynthesis;
  const voices = synth.getVoices();
  const initVoice = voices[173];
  const [isPaused, setIsPaused] = useState(false);
  const [utterance, setUtterance] = useState(null);
  const [voice, setVoice] = useState(initVoice);
  const [pitch, setPitch] = useState(1);
  const [rate, setRate] = useState(0.85);
  const [volume, setVolume] = useState(1);

  useEffect(() => {
    const u = new SpeechSynthesisUtterance(text);
    setUtterance(u);
    
    getVoices().then((voices) => {
      setVoice(initVoice);
    })
  
    return () => {
      synth.cancel();
    };
  }, [text]);

  const handlePlay = async () => {

    if (isPaused) {
      synth.resume();
    } else {
      await getVoices().then((voices) => {

        utterance.voice =  voices.find((v) => v.name === "Microsoft Gadis Online (Natural) - Indonesian (Indonesia)")
        utterance.pitch = pitch;
        utterance.rate = rate;
        utterance.volume = volume;
        synth.speak(utterance);
      });
    }

    setIsPaused(false);
  };

  const handlePause = () => {

    synth.pause();

    setIsPaused(true);
  };

  const handleStop = () => {
    synth.cancel();

    setIsPaused(false);
  };

  const handleVoiceChange = (event) => {
   setVoice(voices.find((v) => v.name === event.target.value));
  };

  const handlePitchChange = (event) => {
    setPitch(parseFloat(event.target.value));
  };

  const handleRateChange = (event) => {
    setRate(parseFloat(event.target.value));
  };

  const handleVolumeChange = (event) => {
    setVolume(parseFloat(event.target.value));
  };

  return (
    <div>
      <label>
        Voice:
        <select value={voice?.name} onChange={handleVoiceChange}>
          {voices.map((voice) => (
            <option key={voice.name} value={voice.name}>
              {voice.name}
            </option>
          ))}
        </select>
      </label>

      <br />

      <label>
        Pitch:
        <input
          type="range"
          min="0.5"
          max="2"
          step="0.1"
          value={pitch}
          onChange={handlePitchChange}
        />
      </label>

      <br />

      <label>
        Speed:
        <input
          type="range"
          min="0.5"
          max="2"
          step="0.1"
          value={rate}
          onChange={handleRateChange}
        />
      </label>
      <br />
      <label>
        Volume:
        <input
          type="range"
          min="0"
          max="1"
          step="0.1"
          value={volume}
          onChange={handleVolumeChange}
        />
      </label>

      <br />

      <button ref={playRef}  onClick={handlePlay}>{isPaused ? "Resume" : "Play"}</button>
      <button onClick={handlePause}>Pause</button>
      <button onClick={handleStop}>Stop</button>
    </div>
  );
};

export default TextToSpeech;