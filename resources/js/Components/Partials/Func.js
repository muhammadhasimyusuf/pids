export function isFiveMinutesBefore(targetTime, now, setIsAlertShown) {
    // Mendapatkan waktu sekarang
    let currentHours = now.getHours().toString().padStart(2, "0");
    let currentMinutes = now.getMinutes().toString().padStart(2, "0");
    let currentTime = `${currentHours}:${currentMinutes}`;

    // Mendapatkan waktu 5 menit sebelum waktu target
    let [targetHours, targetMinutes] = targetTime.split(":").map(Number);
    let targetDate = new Date();
    targetDate.setHours(targetHours);
    targetDate.setMinutes(targetMinutes);
    targetDate.setMinutes(targetDate.getMinutes() - 5);

    let fiveMinutesBeforeHours = targetDate
        .getHours()
        .toString()
        .padStart(2, "0");
    let fiveMinutesBeforeMinutes = targetDate
        .getMinutes()
        .toString()
        .padStart(2, "0");
    let fiveMinutesBefore = `${fiveMinutesBeforeHours}:${fiveMinutesBeforeMinutes}`;
    // setIsAlertShown(true)
    if (currentTime > fiveMinutesBefore) {
        setIsAlertShown(false);
    }
    return currentTime === fiveMinutesBefore;
}

export function timeCheck(
    targetTime,
    now,
    sub = 10,
    setIsAlertShown = () => {}
) {
    // Mendapatkan waktu sekarang
    let currentHours = now.getHours().toString().padStart(2, "0");
    let currentMinutes = now.getMinutes().toString().padStart(2, "0");
    let currentTime = `${currentHours}:${currentMinutes}`;

    // Mendapatkan waktu 5 menit sebelum waktu target
    let [targetHours, targetMinutes] = targetTime.split(":").map(Number);
    let targetDate = new Date();
    targetDate.setHours(targetHours);
    targetDate.setMinutes(targetMinutes);
    targetDate.setMinutes(targetDate.getMinutes() - parseInt(sub));

    let timeCheckHours = targetDate.getHours().toString().padStart(2, "0");
    let timeCheckMinutes = targetDate.getMinutes().toString().padStart(2, "0");
    let timeCheck = `${timeCheckHours}:${timeCheckMinutes}`;
    // setIsAlertShown(true)

    let currentD = new Date();
    let curr = new Date();
    curr.setHours(currentHours, currentMinutes, 0); // 5.30 pm
    let target = new Date();
    target.setHours(timeCheckHours, timeCheckMinutes, 0);
    if ( curr > target) {
        setIsAlertShown && setIsAlertShown(false);
    }
    return currentTime === timeCheck;
}
