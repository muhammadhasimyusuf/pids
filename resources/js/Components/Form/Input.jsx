import { DatePicker, Select, Skeleton } from "antd";
import dayjs from "dayjs";
import React from "react";import advancedFormat from 'dayjs/plugin/advancedFormat'
import customParseFormat from 'dayjs/plugin/customParseFormat'
import localeData from 'dayjs/plugin/localeData'
import weekday from 'dayjs/plugin/weekday'
import weekOfYear from 'dayjs/plugin/weekOfYear'
import weekYear from 'dayjs/plugin/weekYear'

dayjs.extend(customParseFormat)
dayjs.extend(advancedFormat)
dayjs.extend(weekday)
dayjs.extend(localeData)
dayjs.extend(weekOfYear)
dayjs.extend(weekYear)

function Input({
  className = "w-full",
  label,
  altLabel,
  value,
  name,
  id,
  index,
  type = "text",
  onClick,
  onChange,
  onBlur,
  showMagnifier = false,
  magnifierPosition = 'right',
  inputType = 'input', /* input | currency | select | textArea | datePicker */
  children,
  placeholder,
  readOnly,
  fontSize,
disabled = false,
required = false,
addtionalClass,
autoComplete = 'on', 
...dataprops}) {

  return (
    <div className={className}>
       {label && (
          <label htmlFor={id} className={`${altLabel ? 'flex justify-between' : 'block'} text-sm font-medium mt-1`}>
           {label != 'hidden' ?
           <>
            <div>
              {label}
              {required && <span className="text-rose-500">* </span>}
            </div>
            <div>
              {altLabel}
            </div>
           </>
            :
            <div>
               &nbsp; 
            </div>
            }
          </label>
        )}
      <div className="relative">
        {
        // !value && 
        showMagnifier && (
          <div className={`absolute inset-y-0 ${magnifierPosition === 'right' && 'right-0 pl-3'} flex items-center pointer-events-none`}>
            <svg className="w-4 h-4 shrink-0 fill-current text-slate-400 group-hover:text-slate-500 ml-3 mr-2">
              <path d="M7 14c-3.86 0-7-3.14-7-7s3.14-7 7-7 7 3.14 7 7-3.14 7-7 7zM7 2C4.243 2 2 4.243 2 7s2.243 5 5 5 5-2.243 5-5-2.243-5-5-5z" />
              <path d="M15.707 14.293L13.314 11.9a8.019 8.019 0 01-1.414 1.414l2.393 2.393a.997.997 0 001.414 0 .999.999 0 000-1.414z" />
            </svg>
          </div>
        )}
        {
        inputType == 'input' ?
        <input
        onInvalid={e => required && e.target.setCustomValidity(`Input ${label ?? (name ??'field')}`)}
        onInput={e => required && e.target.setCustomValidity('')}
        readOnly={showMagnifier ? true : readOnly}
        disabled={disabled}
        required={required}
        type={type == 'number'? 'text' : type}
        className={`form-input w-full ${showMagnifier && 'cursor-pointer'} text-${fontSize} ${addtionalClass} ${disabled && "bg-gray-100"}`}
        name={name}
        id={id}
        data-key={id}
        data-index={index}
        placeholder={placeholder}
        onClick={onClick}
        onChange={onChange}
        onBlur={onBlur}
        onMouseOver={e => e.target.setCustomValidity('')}
        onBlurCapture={e => e.target.setCustomValidity('')}
        onTouchStart={e => e.target.setCustomValidity('')}
        
        value={value ?? ""}
        title={value ?? ""}
        autoComplete={showMagnifier ? 'off' : type == 'number'? 'off' : autoComplete}
          {...dataprops}
        />
        : inputType == 'select' ?
        <select
        onInvalid={e => required && e.target.setCustomValidity(`Input ${label ?? 'field'}`)}
        onInput={e => required && e.target.setCustomValidity('')}
        readOnly={readOnly}
        disabled={disabled}
        required={required}
          type={type}
          className={`form-input w-full text-${fontSize} ${addtionalClass} ${disabled && "bg-gray-100"}`}
          name={name}
          id={id}
          data-key={id}
          data-index={index}
          onClick={onClick}
          onChange={onChange}
          value={value ?? ""}
          title={value ?? ""}
        >
            {children}
        </select>
        : inputType == 'selectAntd' ?
        <Select
        onInvalid={e => required && e.target.setCustomValidity(`Input ${label ?? 'field'}`)}
        onInput={e => required && e.target.setCustomValidity('')}
        readOnly={readOnly}
        disabled={disabled}
        required={required}
          className={`w-full text-${fontSize} ${addtionalClass} ${disabled && "bg-gray-100"}`}
          name={name}
          id={id}
          onClick={onClick}
          onChange={onChange}
          value={value ?? ""}
          title={value ?? ""}
          {...dataprops}
        />

        : inputType == 'textArea' ?
        <textarea
        onInvalid={e => required && e.target.setCustomValidity(`Input ${label ?? 'field'}`)}
        onInput={e => required && e.target.setCustomValidity('')}
        readOnly={readOnly}
        disabled={disabled}
        required={required}
        type={type}
        className={`form-input w-full text-${fontSize} ${addtionalClass} ${disabled && "bg-gray-100"}`}
        name={name}
        id={id}
        data-key={id}
        data-index={index}
        placeholder={placeholder}
        onClick={onClick}
        onChange={onChange}
        onBlur={onBlur}
        onMouseOver={e => e.target.setCustomValidity('')}
        onBlurCapture={e => e.target.setCustomValidity('')}
        onTouchStart={e => e.target.setCustomValidity('')}
        
        value={value ?? ""}
        title={value ?? ""}
        autoComplete={autoComplete}
        {...dataprops}
        ></textarea>
        :
         inputType == 'datePicker' ?
        <DatePicker
        // popupStyle={{ 
        //   backgroundColor : 'red'
        //  }}
        size="large"
        style={{ fontSize : '11px' }}
        changeOnBlur={true}
         color={'red'}
         key={'red'}
        onInvalid={e => required && e.target.setCustomValidity(`Input ${label ?? 'field'}`)}
        onInput={e => required && e.target.setCustomValidity('')}
          readOnly={readOnly}
          showNow={false}
          popupClassName="bg-red text-black "
          disabled={disabled}
          required={required}
          className={`form-input w-full  ${showMagnifier && 'cursor-pointer'}  ${addtionalClass} ${disabled && "bg-gray-100"}`}
          name={name}
          id={id}
          data-key={id}
          data-index={index}
          placeholder={placeholder}
          onClick={onClick}
          onChange={onChange}
          onBlur={onBlur}
          onMouseOver={e => e.target.setCustomValidity('')}
          onBlurCapture={e => e.target.setCustomValidity('')}
          onTouchStart={e => e.target.setCustomValidity('')}
          
          value={value != '' ? dayjs(value) : ''}
          title={value ?? ""}
          autoComplete={autoComplete}
          {...dataprops}
        />
        :
        ''
    }   
      </div>
    </div>
  );
}




export default Input
