export const pairData = ({ setData =  false, sourceFrom, val }) => {
  return [
    {
      text: 'Jam Kegiatan',
      id: 'datetime',
      addtionalClass: 'md:w-full sm:w-[25%]',
      get value() {
        return val ? val[this.id] : '';
      },
    },
    {
      text: 'Kegiatan',
      id: 'activity_description',
      addtionalClass: '',
      get value() {
        return val ? val[this.id] : '';
      },
    },
    {
      text: 'Keterangan',
      id: 'note',
      addtionalClass: '',
      get value() {
        return val ? val[this.id] : '';
      },
    },
  ];
};